var map = L.map('main_map').setView([6.2660573,-75.5660314], 15);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);


//Llamada a ajax, req asincronico para una pagina
$.ajax({
    dataTyper: 'json',
    url: 'api/bicicletas',
    success: function(result){
        result.bicicletas.forEach(function(bici){
            L.marker(bici.ubicacion, {title: bici.id}).addTo(map);
        })
    }
})
