var mongoose = require('mongoose');
var Reserva = require('./reserva');
const bcrypt = require('bcrypt');
var uniqueValidator = require('mongoose-unique-validator');
var Schema = mongoose.Schema;

const saltRounds = 10;
const validateEmail = function(email){
    const re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    return re.test(email);
};

var usuarioSchema = new Schema({
    nombre: {
        type: String,
        trin: true,
        required: [true, "El nombre es obligatorio"]
    },
    email:{
        type: String,
        trin: true,
        required: [true, "El email es obligatorio"],
        unique: true,
        lowercase: true,
        validate: [validateEmail, "Por favor, ingrese un email valido"],
        match: [/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/]
    },
    password: {
        type: String,
        required: [true, 'La contraseña es obligatoria']
    },
    passwordResetToken: String,
    passwordResetTokenExpires: Date,
    verificado:{
        type: Boolean,
        default: false,
    }
});

usuarioSchema.plugin( uniqueValidator, {message: "El {PATCH} ya existe con otro usuario"});

usuarioSchema.pre('save',function(next){
    if (this.isModified('password')){
        this.password = bcrypt.hashSync(this.password, saltRounds);
    }
    next()
});

usuarioSchema.methods.validatePassword = function(password){
    return bcrypt.compareSync(password,this.password);
};
    

usuarioSchema.methods.reservar = function(biciId, desde, hasta, cb){
    var reserva = new Reserva({usuario: this._id,bicicleta:biciId, desde: desde, hasta: hasta});
    console.log(reserva);
    reserva.save(cb);
};

module.exports = mongoose.model('Usuario', usuarioSchema);