var Bicicleta = require('../../models/bicicleta');
var request = require('request');
var server = require('../../bin/www');

describe('Bicicleta API', ()=>{
    describe('GET BICICLETA /', ()=>{
        it('status 200',() =>{
            expect(Bicicleta.allBicis.length).toBe(0);

            var a = new Bicicleta(1, 'negro', 'urbana', [6.268366, -75.569807]);
            Bicicleta.add(a);

            request.get('http://localhost:3000/api/bicicletas', function(error, response, body){
                expect(response.statusCode).toBe(200);
            })
        });
    });
    describe('POST BICICLETA/creat', ()=>{
        it('status 200',(done) =>{
            var headers = {'content-type':'application/json'};
            var aBici = '{"id":10, "color":"negro","modelo":"montaña","lat":6.268300,"lng":-75.598007}';

            request.post({
                    headers:headers,
                    url:'http://localhost:3000/api/bicicletas/create',
                    body: aBici
                }, function(error, response, body){
                    expect(response.statusCode).toBe(200);
                    expect(Bicicleta.findById(10).color).toBe("negro");
                    done();
            });
        });
    });
    describe('DELETE BICICLETA/delete', ()=>{
        it('status 204',(done) =>{
            var biciId = '{"id":10}';
            request.delete({
                    url:'http://localhost:3000/api/bicicletas/delete',
                    body: biciId
                }, function(error, response, body){
                    expect(response.statusCode).toBe(204);
                    expect(Bicicleta.findById(10)).toBe(false);
                    done();
            });
        });
    });
    describe('UPDATE BICICLETA/update', ()=>{
        it('status 200',(done) =>{
            var headers = {'content-type':'application/json'};
            var aBici = '{"id":10, "color":"rojo","modelo":"montaña","lat":6.268300,"lng":-75.598007}';
            request.post({
                    headers:headers,
                    url:'http://localhost:3000/api/bicicletas/update',
                    body: aBici
                }, function(error, response, body){
                    expect(response.statusCode).toBe(200);
                    expect(Bicicleta.findById(10).color).toBe("rojo");
                    done();
            });
        });
    });
});