var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');

describe('Testing Bicicletas', function(){
    beforeEach(function(done){
        var mongoDB = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDB,{useNewUrlParser : true});

        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', function(){
            console.log('We are connected to test database');
            done();
        });
    });

    afterEach(function(done){
        Bicicleta.deleteMany({}, function(err, success){
            if (err) console.log(err);
            done();
        });
    });

    describe('Bicicleta createInstance', ()=> {
        it('Crear una instancia de una bicicleta', ()=>{
            var bici = Bicicleta.createInstance(1,'verde','montana',[6.268366, -75.569807]);

            expect(bici.code).toBe(1);
            expect(bici.color).toBe('verde');
            expect(bici.modelo).toBe('montaña');
            expect(bici.ubicacion[0]).toEqual(6.268366);            
            expect(bici.ubicacion[1]).toEqual(-75.569807);
        });
    });
    describe('Bicicleta.allBicis',() =>{
        it('comenzar vacio',(done) => {
            Bicicleta.allBicis(function(err, bicis){
                expect(bicis.length).toEqual(0);
                done();
            });
        });
    }); 
    describe('Bicicleta.add',()=>{
        it('agregar solo una bici',(done)=>{
            var aBici = new Bicicleta({code:1, color:'verde', modelo:'urbana'});
            Bicicleta.addListener(aBici, function(err,newBici){
                if(err) console.log(err);
                Bicicleta.allBicis(function(err, bicis){
                    expect(bicis.length).toEqual(1);
                    expect(bicis[0].color).toBe(aBici.color);

                    done();
                });
            });
        });
    });
    describe('Bicicleta.findByCode',()=>{
        it('Devolver la bici con code:1',(done)=>{
            Bicicleta.allBicis(function(err,bicis){
                expect(bicis.length).toBe(0);

                var aBici = new Bicicleta({code:1, color:'negro',modelo:'urbana'});
                Bicicleta.add(aBici, function(err,newBici){
                    if(err) console.log(err);
                    var bBici = new Bicicleta({code:2, color:'rojo',modelo:'urbana'});
                    Bicicleta.add(bBici, function(err,newBici){
                        if(err) console.log(err);
                        Bicicleta.findByCode(1, function(err, targetBici){
                            expect(targetBici.code).toBe(aBici.code);
                            expect(targetBici.color).toBe(aBici.color);
                            expect(targetBici.modelo).toBe(aBici.modelo);

                            done();
                        });
                    });
                });                
            });
        });
    });
    describe('Bicicleta.removeByCode',()=>{
        it('Debe eliminar la bici con code:1',(done)=>{
            Bicicleta.allBicis(function(cb,bicis){
                expect(bicis.length).toBe(0);

                var aBici = new Bicicleta({code:1, color:'negro',modelo:'urbana'});
                Bicicleta.add(aBici, function(err,newBici){
                    if(err) console.log(err);

                    Bicicleta.removeByCode(1,function(err,bicis){
                        expect(bicis.length).toEqual(0);

                    });
                });
            });
        });
    });
});




/*
beforeEach(()=>{Bicicleta.allBicis = []; });
describe('Bicicleta.allBicis',() =>{
    it('comenzar vacio',() =>{
        expect(Bicicleta.allBicis.length).toBe(0);
    });
});

describe('Bicicleta.add', () =>{
     it('agregamos una', ()=>{
         expect(Bicicleta.allBicis.length).toBe(0); //Pre condicion
         var a = new Bicicleta(1, 'rojo', 'urbana', [6.268366, -75.569807]);
         Bicicleta.add(a);

         expect(Bicicleta.allBicis.length).toBe(1);
         expect(Bicicleta.allBicis[0]).toBe(a); //Post condicion
     });
}); 

describe('Bicicleta.findById',() =>{
    it('Debe devolver bici con ID 1',()=>{
        expect(Bicicleta.allBicis.length).toBe(0);
        var aBici1 = new Bicicleta(1, 'rojo', 'urbana', [6.268366, -75.569807]);
        var aBici2 = new Bicicleta(2,'amarilla', 'urbana', [6.266297, -75.569560]);
        Bicicleta.add(aBici1);
        Bicicleta.add(aBici2);

        var targetBici = Bicicleta.findById(1);

        expect(targetBici.id).toBe(1);
        expect(targetBici.color).toBe(aBici1.color);
        expect(targetBici.modelo).toBe(aBici1.modelo);
    })
})
*/